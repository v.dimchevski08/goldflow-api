﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace GoldFlowAPI.Utilities
{
    public static class EncryptionHelper
    {
        private static string EncryptionKey = "Yp3s5v8y/B?E(H+MbQeThWmZq4t7w9z$";

        #region Aes
        private static Aes CreateAes()
        {
            Aes aes = Aes.Create();

            aes.Padding = PaddingMode.PKCS7;
            aes.Mode = CipherMode.CBC;
            aes.IV = new byte[16];
            aes.KeySize = 256;
            aes.Key = Encoding.UTF8.GetBytes(EncryptionKey);

            return aes;
        }
        #endregion

        #region Encryption
        public static string Encrypt(string text)
        {
            Aes aes = CreateAes();

            ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

            using(MemoryStream memoryStream = new MemoryStream())
            {
                using(CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    using(StreamWriter streamWriter = new StreamWriter(cryptoStream))
                    {
                        streamWriter.Write(text);
                    }
                }

                return Convert.ToBase64String(memoryStream.ToArray());
            }
        }
        #endregion

        #region Decryption
        public static string Decrypt(string encryptedText)
        {
            byte[] cipherBytes = Convert.FromBase64String(encryptedText);

            Aes aes = CreateAes();

            ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

            using(MemoryStream memoryStream = new MemoryStream(cipherBytes))
            {
                using(CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                {
                    using(StreamReader streamReader = new StreamReader(cryptoStream))
                    {
                        return streamReader.ReadToEnd();
                    }
                }
            }
        }
        #endregion
    }
}
