﻿using DTOs;
using Microsoft.AspNetCore.Mvc;

namespace GoldFlowAPI.Utilities
{
    public static class ResponseHelper<T>
    {
        public static ObjectResult Success(T data)
        {
            return new ObjectResult(new Response<T>(data));
        }

        public static BadRequestObjectResult IncorrectInput(T data)
        {
            return new BadRequestObjectResult(new Response<T>(data, 400, true, "Incorrect input. Please check the mandatory fields and the input rules!"));
        }

        public static NotFoundObjectResult UnexistingResource(T data)
        {
            return new NotFoundObjectResult(new Response<T>(data, 404, true, "The resource is unexisting!"));
        }
    }
}
