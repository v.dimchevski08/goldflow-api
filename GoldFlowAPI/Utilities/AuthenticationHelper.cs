﻿using GoldFlowAPI.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace GoldFlowAPI.Utilities
{
    public static class AuthenticationHelper
    {
        #region Generating Token
        public static string GenerateToken(User user)
        {
            if(user != null)
            {
                string userId = EncryptionHelper.Encrypt(user.Id.ToString());

                Claim[] claims = new Claim[] { new Claim("UserId", userId) };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Startup.TokenKey));

                var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);

                JwtSecurityToken jwt = new JwtSecurityToken
                    (
                        issuer: Startup.Issuer,
                        audience: Startup.Audience,
                        expires: DateTime.Now.AddHours(24),
                        claims: claims,
                        signingCredentials: credentials
                    );

                return new JwtSecurityTokenHandler().WriteToken(jwt);
            };

            return string.Empty;
        }
        #endregion
    }
}
