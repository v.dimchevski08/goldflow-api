﻿using DTOs;
using GoldFlowAPI.Filters;
using GoldFlowAPI.Services;
using GoldFlowAPI.Utilities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoldFlowAPI.Controllers
{
    [Route("user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService service;
        private readonly IEmailSender sender;

        public UserController(IUserService _service, IEmailSender _sender)
        {
            service = _service;
            sender = _sender;
        }

        #region GET

        /// <summary>
        /// Get specific User by ID
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET user/id
        ///     
        /// 
        /// </remarks>
        /// <param name="id">ID of the specific User</param>
        /// <returns>Specific User</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [Authenticate]
        [HttpGet]
        public async Task<IActionResult> GetUser(string id)
        {
            if(service.GetUser(id).Result == null)
            {
                return ResponseHelper<UserDTO>.UnexistingResource(new UserDTO());
            }

            return ResponseHelper<UserDTO>.Success(await service.GetUser(id));
        }

        /// <summary>
        /// Get list of all Users
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET user/list
        ///     
        /// 
        /// </remarks> 
        /// <returns>List of all Users</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [HttpGet("list")]
        public async Task<IActionResult> GetAll()
        {
            if (service.GetAll().Result == null || !service.GetAll().Result.Any())
            {
                return ResponseHelper<List<UserDTO>>.UnexistingResource(new List<UserDTO>());
            }

            return ResponseHelper<List<UserDTO>>.Success(await service.GetAll());
        }

        #endregion

        #region Datatables

        /// <summary>
        /// Get datatable of Users
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET user/datatable
        ///     
        ///     {
        ///         "pageSize": 1,
        ///         "pageNumber": 1,
        ///         "sortColumn": "category",
        ///         "sortDirection": "asc"
        ///     }
        /// 
        /// *Mandatory fields: None
        /// </remarks> 
        /// <returns>Datatable of Users</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [Authenticate]
        [HttpPost("datatable")]
        public async Task<IActionResult> UsersDatatable([FromBody] PaginationFilter filter)
        {
            if (service.UsersDatatable(filter).Result.Data == null
                || !service.UsersDatatable(filter).Result.Data.Any())
            {
                return ResponseHelper<List<UserDTO>>.UnexistingResource(new List<UserDTO>());
            }

            return new ObjectResult(await service.UsersDatatable(filter));
        }

        #endregion

        #region POST

        /// <summary>
        /// Create User
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST user/create
        ///     {
        ///         "category": "Team Member",
        ///         "firstName": "John",
        ///         "lastName": "Doe",
        ///         "email": "user@provider.com",
        ///         "password": "y0ur50nlY"
        ///     }
        /// 
        /// *Mandatory Fields: All fields are mandatory
        /// </remarks>
        /// <param name="user">User's data</param>
        /// <returns>ID of the newly created user</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="400">Indicates that the input was incorrect</response>
        [Authenticate]
        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody] CreateUserDTO user)
        {
            if (string.IsNullOrEmpty(user.Category)
                || string.IsNullOrEmpty(user.FirstName)
                || string.IsNullOrEmpty(user.LastName)
                || string.IsNullOrEmpty(user.Email)
                || string.IsNullOrEmpty(user.Password))
            {
                return ResponseHelper<CreateUserDTO>.IncorrectInput(user);
            }

            if (user.Category == "Project Manager" || user.Category == "Team Member")
            {
                return ResponseHelper<string>.Success(await service.Create(user));
            }

            return ResponseHelper<string>.IncorrectInput(user.Category);
        }

        #endregion

        #region PUT

        /// <summary>
        /// Update User
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST user/update
        ///     {
        ///         "Id": "wuwskkqj2821==sk"
        ///         "category": "Team Member",
        ///         "firstName": "John",
        ///         "lastName": "Doe",
        ///         "email": "user@provider.com",
        ///         "password": "y0ur50nlY"
        ///     }
        /// 
        /// *Mandatory Fields: All fields are mandatory
        /// </remarks>
        /// <param name="user">User's data</param>
        /// <returns>True if the request has succeeded, otherwise false</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="400">Indicates that the input was incorrect</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [Authenticate]
        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody] UpdateUserDTO user)
        {
            if(service.GetUser(user.Id).Result == null)
            {
                return ResponseHelper<UpdateUserDTO>.UnexistingResource(new UpdateUserDTO());
            }

            if (string.IsNullOrEmpty(user.Category)
               || string.IsNullOrEmpty(user.FirstName)
               || string.IsNullOrEmpty(user.LastName)
               || string.IsNullOrEmpty(user.Email)
               || string.IsNullOrEmpty(user.Password)
               )
            {
                return ResponseHelper<UpdateUserDTO>.IncorrectInput(user);
            }

            return ResponseHelper<bool>.Success(await service.Update(user));
        }

        #endregion

        #region DELETE

        /// <summary>
        /// Delete User
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET user/delete/id
        ///     
        /// 
        /// </remarks>
        /// <param name="id">ID of the specific User</param>
        /// <returns>True if the request has succeeded, otherwise false</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [Authenticate]
        [HttpPost("delete")]
        public async Task<IActionResult> DeleteUser(string id)
        {
            if(service.GetUser(id).Result == null)
            {
                return ResponseHelper<UserDTO>.UnexistingResource(new UserDTO());
            }

            return ResponseHelper<bool>.Success(await service.DeleteUser(id));
        }

        #endregion

        #region LOGIN

        /// <summary>
        /// User Log In
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST user/login
        ///     {
        ///         "email": "user@provider.com",
        ///         "password": "y0ur50nlY"
        ///     }
        /// 
        /// *Mandatory Fields: Both fields are Mandatory
        /// </remarks>
        /// <param name="request">Request's data</param>
        /// <returns>User's details and Token</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="400">Indicates that the input was incorrect</response>
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginRequestDTO request)
        {
            if(string.IsNullOrEmpty(request.Email) || string.IsNullOrEmpty(request.Password))
            {
                return ResponseHelper<LoginRequestDTO>.IncorrectInput(request);
            }
            
            return ResponseHelper<LoginResponseDTO>.Success(await service.Login(request));
        }

        #endregion

        #region Email

        /// <summary>
        /// Send Email
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST user/send/email
        ///     
        ///     {
        ///         "from": "sender@email.com",
        ///         "to": ["recipient1@email.com", "recipient2@email.com", "recipient3@email.com"],
        ///         "subject": "The Actual Subject",
        ///         "message": "The Actual Message"
        ///      }
        /// 
        /// *Mandatory Fields: From, To, Message
        /// </remarks>
        /// <param name="email">Email's data</param>
        /// <returns>True if the request has succeeded, otherwise false</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="400">Indicates that the input was incorrect</response>
        [Authenticate]
        [HttpPost("send/email")]
        public async Task<IActionResult> SendEmail([FromBody] Email email)
        {
            if (string.IsNullOrEmpty(email.From)
                || email.To == null
                || !email.To.Any()
                || string.IsNullOrEmpty(email.Message))
            {
                return ResponseHelper<Email>.IncorrectInput(email);
            }

            return ResponseHelper<bool>.Success(await sender.SendEmail(email.From, email.To, email.Subject, email.Message));
        }

        #endregion
    }
}
