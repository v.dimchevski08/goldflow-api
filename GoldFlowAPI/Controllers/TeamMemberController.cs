﻿using DTOs;
using GoldFlowAPI.Filters;
using GoldFlowAPI.Services;
using GoldFlowAPI.Utilities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoldFlowAPI.Controllers
{
    [Route("teammember")]
    [ApiController]
    public class TeamMemberController : ControllerBase
    {
        private readonly ITeamMemberService service;
        private readonly IUserService userService;

        public TeamMemberController(ITeamMemberService _service, IUserService _userService)
        {
            service = _service;
            userService = _userService;
        }

        #region GET

        /// <summary>
        /// Get list of all Team Members
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET teammember/list
        ///     
        /// 
        /// </remarks> 
        /// <returns>List of all Team Members</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [Authenticate]
        [HttpGet("list")]
        public async Task<IActionResult> GetAll()
        {
            if(service.GetAll().Result == null || !service.GetAll().Result.Any())
            {
                return ResponseHelper<List<TeamMemberDTO>>.UnexistingResource(new List<TeamMemberDTO>());
            }

            return ResponseHelper<List<TeamMemberDTO>>.Success(await service.GetAll());
        }

        #endregion

        #region POST

        /// <summary>
        /// Add Team Member
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST teammember/add
        ///     {
        ///         "role": "Full Stack Developer",
        ///         "userId": "urrhshhw/hr/rt23/123==",
        ///         "teamId": "dhsjl1l1k*shh/292=="
        ///     }
        /// 
        /// *Mandatory Fields: Role, User ID, Team ID
        /// </remarks>
        /// <param name="teamMember">Team Member's data</param>
        /// <returns>ID of the newly added Team Member</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="400">Indicates that the input was incorrect</response>
        [Authenticate]
        [HttpPost("add")]
        public async Task<IActionResult> Create([FromBody] CreateTeamMemberDTO teamMember)
        {
            if (userService.GetUser(teamMember.UserId).Result == null)
            {
                return ResponseHelper<UserDTO>.UnexistingResource(new UserDTO());
            }

            if (string.IsNullOrEmpty(teamMember.Role))
            {
                return ResponseHelper<string>.IncorrectInput(teamMember.Role);
            }

            return ResponseHelper<string>.Success(await service.Create(teamMember));
        }

        #endregion

        #region Datatables

        /// <summary>
        /// Get datatable of Team Members
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET teammember/datatable
        ///     
        ///     {
        ///         "pageSize": 1,
        ///         "pageNumber": 1,
        ///         "sortColumn": "role",
        ///         "sortDirection": "desc"
        ///     }
        /// 
        /// *Mandatory fields: None
        /// </remarks> 
        /// <returns>Datatable of Team Members</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [Authenticate]
        [HttpPost("datatable")]
        public async Task<IActionResult> TeamMembersDatatable(PaginationFilter filter)
        {
            if(service.TeamMembersDatatable(filter).Result.Data == null
               || !service.TeamMembersDatatable(filter).Result.Data.Any())
            {
                return ResponseHelper<List<TeamMemberDTO>>.UnexistingResource(new List<TeamMemberDTO>());
            }

            return new ObjectResult(await service.TeamMembersDatatable(filter));
        }

        #endregion
    }
}
