﻿using DTOs;
using GoldFlowAPI.Filters;
using GoldFlowAPI.Services;
using GoldFlowAPI.Utilities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoldFlowAPI.Controllers
{
    [Route("project")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectService service;
        private readonly IUserService userService;

        public ProjectController(IProjectService _service, IUserService _userService)
        {
            service = _service;
            userService = _userService;
        }

        #region GET

        /// <summary>
        /// Get specific Project by ID
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET project/id
        ///     
        /// 
        /// </remarks>
        /// <param name="id">ID of the specific Project</param>
        /// <returns>Specific Project</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [Authenticate]
        [HttpGet]
        public async Task<IActionResult> GetTask(string id)
        {
            if (service.GetProject(id).Result == null)
            {
                return ResponseHelper<ProjectDTO>.UnexistingResource(new ProjectDTO());
            }

            return ResponseHelper<ProjectDTO>.Success(await service.GetProject(id));
        }

        /// <summary>
        /// Get list of all Projects
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET project/list
        ///     
        /// 
        /// </remarks> 
        /// <returns>List of all Projects</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [Authenticate]
        [HttpGet("list")]
        public async Task<IActionResult> GetAll()
        {
            if (service.GetAll().Result == null || !service.GetAll().Result.Any())
            {
                return ResponseHelper<List<ProjectDTO>>.UnexistingResource(new List<ProjectDTO>());
            }

            return ResponseHelper<List<ProjectDTO>>.Success(await service.GetAll());
        }

        /// <summary>
        /// Get list of Unassigned Projects
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET project/list/unassigned
        ///     
        /// 
        /// </remarks> 
        /// <returns>List of Unassigned Projects</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [HttpGet("list/unassigned")]
        public async Task<IActionResult> GetUnassigned()
        {
            if(service.GetUnassigned().Result == null || !service.GetUnassigned().Result.Any())
            {
                return ResponseHelper<List<ProjectDTO>>.UnexistingResource(new List<ProjectDTO>());
            }

            return ResponseHelper<List<ProjectDTO>>.Success(await service.GetUnassigned());
        }

        #endregion

        #region Datatables

        /// <summary>
        /// Get datatable of Projects
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET project/datatable
        ///     
        ///     {
        ///         "pageSize": 1,
        ///         "pageNumber": 1,
        ///         "sortColumn": "status",
        ///         "sortDirection": "asc"
        ///     }
        /// 
        /// *Mandatory fields: None
        /// </remarks> 
        /// <returns>Datatable of Projects</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [Authenticate]
        [HttpPost("datatable")]
        public async Task<IActionResult> ProjectsDatatable([FromBody] PaginationFilter filter)
        {
            if (service.ProjectsDatatable(filter).Result.Data == null
                || !service.ProjectsDatatable(filter).Result.Data.Any())
            {
                return ResponseHelper<List<ProjectDTO>>.UnexistingResource(new List<ProjectDTO>());
            }

            return new ObjectResult(await service.ProjectsDatatable(filter));
        }

        /// <summary>
        /// Get datatable of Unassigned Projects
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET project/unassigned/datatable
        ///     
        ///     {
        ///         "pageSize": 1,
        ///         "pageNumber": 1,
        ///         "sortColumn": "status",
        ///         "sortDirection": "asc"
        ///     }
        /// 
        /// *Mandatory fields: None
        /// </remarks> 
        /// <returns>Datatable of Unassigned Projects</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [Authenticate]
        [HttpPost("unassigned/datatable")]
        public async Task<IActionResult> UnassignedProjectsDatatable([FromBody] PaginationFilter filter)
        {
            if (service.UnassignedProjectsDatatable(filter).Result.Data == null
                || !service.UnassignedProjectsDatatable(filter).Result.Data.Any())
            {
                return ResponseHelper<List<ProjectDTO>>.UnexistingResource(new List<ProjectDTO>());
            }

            return new ObjectResult(await service.UnassignedProjectsDatatable(filter));
        }

        #endregion

        #region POST

        /// <summary>
        /// Create Project
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST project/create
        ///     {
        ///         "title": "Project Management Software",
        ///         "status": "Finished",
        ///         "startDate": "2008-11-11 11:12:01",
        ///         "endDate": "2008-12-11 11:12:01",
        ///         "managerId": "dhsjl1l1k*shh/292=="
        ///     }
        /// 
        /// *Mandatory Fields: Title, Status, Start Date, End Date, ManagerId
        /// </remarks>
        /// <param name="project">Project's data</param>
        /// <returns>ID of the newly created project</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="400">Indicates that the input was incorrect</response>
        [Authenticate]
        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody] CreateProjectDTO project)
        {
            if (string.IsNullOrEmpty(project.Title)
                || string.IsNullOrEmpty(project.Status)
                || project.StartDate == null
                || project.EndDate == null
                || string.IsNullOrEmpty(project.ManagerId)
                )
            {
                return ResponseHelper<CreateProjectDTO>.IncorrectInput(project);
            }

            if (userService.GetUser(project.ManagerId).Result == null)
            {
                return ResponseHelper<UserDTO>.UnexistingResource(new UserDTO());
            }

            return ResponseHelper<string>.Success(await service.Create(project));
        }

        #endregion

        #region DELETE

        /// <summary>
        /// Delete Project
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST project/delete/projectId
        ///     
        /// *Mandatory Fields: Project ID
        /// </remarks>
        /// <param name="id">ID of the specific Project</param>
        /// <returns>True if the request has succeeded, otherwise false</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [Authenticate]
        [HttpPost("delete")]
        public async Task<IActionResult> DeleteProject(string id)
        {
            if (service.GetAll().Result.Any(p => p.Id == id))
            {
                return ResponseHelper<bool>.Success(await service.DeleteProject(id));
            }

            return ResponseHelper<ProjectDTO>.UnexistingResource(new ProjectDTO());

        }

        #endregion
    }
}
