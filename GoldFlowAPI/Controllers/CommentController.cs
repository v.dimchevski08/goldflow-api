﻿using DTOs;
using GoldFlowAPI.Filters;
using GoldFlowAPI.Services;
using GoldFlowAPI.Utilities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoldFlowAPI.Controllers
{
    [Route("comment")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly ICommentService service;

        public CommentController(ICommentService _service)
        {
            service = _service;
        }

        #region GET

        /// <summary>
        /// Get list of all Comments
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET comment/list
        ///     
        /// 
        /// </remarks> 
        /// <returns>List of all Comments</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [HttpGet("list")]
        public async Task<IActionResult> GetAll()
        {
            if (service.GetAll().Result == null || !service.GetAll().Result.Any())
            {
                return ResponseHelper<List<CommentDTO>>.UnexistingResource(new List<CommentDTO>());
            }

            return ResponseHelper<List<CommentDTO>>.Success(await service.GetAll());
        }

        #endregion

        #region Datatables

        /// <summary>
        /// Get datatable of Comments
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET comment/datatable
        ///     
        ///     {
        ///         "pageSize": 1,
        ///         "pageNumber": 1,
        ///         "sortColumn": "status",
        ///         "sortDirection": "asc"
        ///     }
        /// 
        /// *Mandatory fields: None
        /// </remarks> 
        /// <returns>Datatable of Comments</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [Authenticate]
        [HttpPost("datatable")]
        public async Task<IActionResult> CommentsDatatable([FromBody] PaginationFilter filter)
        {
            if (service.CommentsDatatable(filter).Result.Data == null
                || !service.CommentsDatatable(filter).Result.Data.Any())
            {
                return ResponseHelper<List<CommentDTO>>.UnexistingResource(new List<CommentDTO>());
            }

            return new ObjectResult(await service.CommentsDatatable(filter));
        }

        #endregion

        #region POST

        /// <summary>
        /// Create Comment
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST comment/create
        ///     {
        ///         "text": "The CSS part is always annoying!",
        ///         "taskId": "55"
        ///     }
        /// 
        /// *Mandatory Fields: Both fields are mandatory
        /// </remarks>
        /// <param name="comment">Comment's data</param>
        /// <returns>ID of the newly created comment</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="400">Indicates that the input was incorrect</response>
        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody] CreateCommentDTO comment)
        {
            if (string.IsNullOrEmpty(comment.Text))
            {
                return ResponseHelper<string>.IncorrectInput(comment.Text);
            }

            return ResponseHelper<string>.Success(await service.Create(comment));
        }

        #endregion

        #region DELETE

        /// <summary>
        /// Delete Comment
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST comment/delete/commentId
        ///     
        /// *Mandatory Fields: Comment ID
        /// </remarks>
        /// <param name="id">ID of the specific Comment</param>
        /// <returns>True if the request has succeeded, otherwise false</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [HttpPost("delete")]
        public async Task<IActionResult> DeleteComment(string id)
        {
            if(service.GetAll().Result.Any(c => c.Id == id))
            {
                return ResponseHelper<bool>.Success(await service.DeleteComment(id));
            }

            return ResponseHelper<CommentDTO>.UnexistingResource(new CommentDTO());
            
        }

        #endregion
    }
}
