﻿using DTOs;
using GoldFlowAPI.Filters;
using GoldFlowAPI.Services;
using GoldFlowAPI.Utilities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoldFlowAPI.Controllers
{
    [Route("team")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly ITeamService service;
        private readonly IProjectService projectService;

        public TeamController(ITeamService _service, IProjectService _projectService)
        {
            service = _service;
            projectService = _projectService;
        }

        #region GET

        /// <summary>
        /// Get list of Teams
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET team/list
        ///     
        /// 
        /// </remarks> 
        /// <returns>List of Teams</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [Authenticate]
        [HttpGet("list")]
        public async Task<IActionResult> GetAll()
        {
            if(service.GetAll().Result == null || !service.GetAll().Result.Any())
            {
                return ResponseHelper<List<TeamDTO>>.UnexistingResource(new List<TeamDTO>());
            }

            return ResponseHelper<List<TeamDTO>>.Success(await service.GetAll());
        }

        #endregion

        #region POST

        /// <summary>
        /// Create Team
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST team/create
        ///     {
        ///         "name": "Web Programming Team",
        ///         "projectId": "urrhshhw/hr/rt23/123=="
        ///     }
        /// 
        /// *Mandatory Fields: Project ID
        /// </remarks>
        /// <param name="team">Team's data</param>
        /// <returns>ID of the newly created Team</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="400">Indicates that the input was incorrect</response>
        [Authenticate]
        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody] CreateTeamDTO team)
        {
            if (string.IsNullOrEmpty(team.ProjectId))
            {
                return ResponseHelper<string>.IncorrectInput(team.ProjectId);
            }

            if (projectService.GetProject(team.ProjectId).Result == null)
            {
                return ResponseHelper<string>.UnexistingResource(team.ProjectId);
            }

            return ResponseHelper<string>.Success(await service.Create(team));
        }

        #endregion

        #region Datatables

        /// <summary>
        /// Get datatable of Teams
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET team/datatable
        ///     
        ///     {
        ///         "pageSize": 1,
        ///         "pageNumber": 1,
        ///         "sortColumn": "teamId",
        ///         "sortDirection": "desc"
        ///     }
        /// 
        /// *Mandatory fields: None
        /// </remarks> 
        /// <returns>Datatable of Teams</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [Authenticate]
        [HttpPost("datatable")]
        public async Task<IActionResult> TeamsDatatable(PaginationFilter filter)
        {
            if(service.TeamsDatatable(filter).Result.Data == null
               || !service.TeamsDatatable(filter).Result.Data.Any())
            {
                return ResponseHelper<List<TeamDTO>>.UnexistingResource(new List<TeamDTO>());
            }

            return new ObjectResult(await service.TeamsDatatable(filter));
        }

        #endregion
    }
}
