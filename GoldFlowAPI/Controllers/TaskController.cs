﻿using DTOs;
using GoldFlowAPI.Filters;
using GoldFlowAPI.Services;
using GoldFlowAPI.Utilities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoldFlowAPI.Controllers
{
    [Route("task")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly ITaskService service;
        private readonly IProjectService projectService;

        public TaskController(ITaskService _service, IProjectService _projectService)
        {
            service = _service;
            projectService = _projectService;
        }

        #region GET

        /// <summary>
        /// Get specific Task by ID
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET task/id
        ///     
        /// 
        /// </remarks>
        /// <param name="id">ID of the specific Task</param>
        /// <returns>Specific Task</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [Authenticate]
        [HttpGet]
        public async Task<IActionResult> GetTask(string id)
        {
            if (service.GetTask(id).Result == null)
            {
                return ResponseHelper<TaskDTO>.UnexistingResource(new TaskDTO());
            }

            return ResponseHelper<TaskDTO>.Success(await service.GetTask(id));
        }

        /// <summary>
        /// Get list of Unassigned Tasks
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET task/list/unassigned
        ///     
        /// 
        /// </remarks> 
        /// <returns>List of Unassigned Tasks</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [HttpGet("list/unassigned")]
        public async Task<IActionResult> GetUnassignedTasks()
        {
            if(service.GetUnassignedTasks().Result == null || !service.GetUnassignedTasks().Result.Any())
            {
                return ResponseHelper<List<TaskDTO>>.UnexistingResource(new List<TaskDTO>());
            }

            return ResponseHelper<List<TaskDTO>>.Success(await service.GetUnassignedTasks());
        }

        /// <summary>
        /// Get list of Tasks by Team Member
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET task/list/teammember
        ///     
        /// 
        /// </remarks> 
        /// <param name="teamMemberId">ID of the specific Team Member</param>
        /// <returns>List of Tasks by Team Member</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [Authenticate]
        [HttpGet("list/teammember")]
        public async Task<IActionResult> GetTasksByTeamMember(string teamMemberId)
        {
            if(service.GetTasksByTeamMember(teamMemberId).Result == null
               || !service.GetTasksByTeamMember(teamMemberId).Result.Any())
            {
                return ResponseHelper<List<TaskDTO>>.UnexistingResource(new List<TaskDTO>());
            }

            return ResponseHelper<List<TaskDTO>>.Success(await service.GetTasksByTeamMember(teamMemberId));
        }

        /// <summary>
        /// Get list of Finished Tasks by Team Member
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET task/finished/list/teammember
        ///     
        /// 
        /// </remarks> 
        /// <param name="teamMemberId">ID of the specific Team Member</param>
        /// <returns>List of Finished Tasks by Team Member</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [Authenticate]
        [HttpGet("finished/list/teammember")]
        public async Task<IActionResult> GetFinishedTasksByTeamMember(string teamMemberId)
        {
            if(service.GetFinishedTasksByTeamMember(teamMemberId).Result == null
               || !service.GetFinishedTasksByTeamMember(teamMemberId).Result.Any())
            {
                return ResponseHelper<List<TaskDTO>>.UnexistingResource(new List<TaskDTO>());
            }

            return ResponseHelper<List<TaskDTO>>.Success(await service.GetFinishedTasksByTeamMember(teamMemberId));
        }

        /// <summary>
        /// Get list of Tasks by Project
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET task/list/project
        ///     
        /// 
        /// </remarks> 
        /// <param name="projectId">ID of the specific Team Member</param>
        /// <returns>List of Tasks by Project</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [Authenticate]
        [HttpGet("list/project")]
        public async Task<IActionResult> GetTasksByProject(string projectId)
        {
            if(service.GetTasksByProject(projectId).Result == null
               || !service.GetTasksByProject(projectId).Result.Any())
            {
                return ResponseHelper<List<TaskDTO>>.UnexistingResource(new List<TaskDTO>());
            }

            return ResponseHelper<List<TaskDTO>>.Success(await service.GetTasksByProject(projectId));
        }
        
        #endregion

        #region Datatables

        /// <summary>
        /// Get datatable of Tasks for a specific Project
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET task/datatable/projectId
        ///     
        ///     {
        ///         "pageSize": 1,
        ///         "pageNumber": 1,
        ///         "sortColumn": "status",
        ///         "sortDirection": "asc"
        ///     }
        /// 
        /// *Mandatory fields: ProjectID
        /// </remarks> 
        /// <param name="projectId">ID of the specific Task</param>
        /// <returns>Datatable of Tasks for a specific Project</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [Authenticate]
        [HttpPost("datatable/project")]
        public async Task<IActionResult> TasksByProjectDatatable([FromBody] PaginationFilter filter, string projectId)
        {
            if (service.TasksByProjectDatatable(filter, projectId).Result.Data == null
               || !service.TasksByProjectDatatable(filter, projectId).Result.Data.Any())
            {
                return ResponseHelper<List<TaskDTO>>.UnexistingResource(new List<TaskDTO>());
            }

            return new ObjectResult(await service.TasksByProjectDatatable(filter, projectId));
        }

        /// <summary>
        /// Get datatable of Unassigned Tasks
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET task/unassigned/datatable
        ///     
        ///     {
        ///         "pageSize": 1,
        ///         "pageNumber": 1,
        ///         "sortColumn": "status",
        ///         "sortDirection": "asc"
        ///     }
        /// 
        /// *Mandatory fields: None
        /// </remarks> 
        /// <returns>Datatable of Unassigned Tasks</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [Authenticate]
        [HttpPost("unassigned/datatable")]
        public async Task<IActionResult> UnassignedTasksDatatable([FromBody] PaginationFilter filter)
        {
            if (service.UnassignedTasksDatatable(filter).Result.Data == null
                || !service.UnassignedTasksDatatable(filter).Result.Data.Any())
            {
                return ResponseHelper<List<TaskDTO>>.UnexistingResource(new List<TaskDTO>());
            }

            return new ObjectResult(await service.UnassignedTasksDatatable(filter));
        }

        /// <summary>
        /// Get datatable of Tasks by Team Member
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET task/teammember/datatable/teamMemberId
        ///     
        ///     {
        ///         "pageSize": 1,
        ///         "pageNumber": 1,
        ///         "sortColumn": "status",
        ///         "sortDirection": "asc"
        ///     }
        /// 
        /// *Mandatory fields: Team Member ID
        /// </remarks> 
        /// <param name="teamMemberId">ID of the specific Team Member</param>
        /// <returns>Datatable of Tasks by Team Member</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [Authenticate]
        [HttpPost("teammember/datatable")]
        public async Task<IActionResult> TasksByTeamMemberDatatable([FromBody] PaginationFilter filter, string teamMemberId)
        {
            if (service.TasksByTeamMemberDatatable(filter, teamMemberId).Result.Data == null
               || !service.TasksByTeamMemberDatatable(filter, teamMemberId).Result.Data.Any())
            {
                return ResponseHelper<List<TaskDTO>>.UnexistingResource(new List<TaskDTO>());
            }

            return new ObjectResult(await service.TasksByTeamMemberDatatable(filter, teamMemberId));
        }

        /// <summary>
        /// Get datatable of Finished Tasks by Team Member
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET task/finished/datatable/teammember
        ///     
        ///     {
        ///         "pageSize": 1,
        ///         "pageNumber": 1,
        ///         "sortColumn": "status",
        ///         "sortDirection": "asc"
        ///     }
        /// 
        /// *Mandatory fields: Team Member ID
        /// </remarks> 
        /// <param name="teamMemberId">ID of the specific Team Member</param>
        /// <returns>Datatable of Finished Tasks by Team Member</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [Authenticate]
        [HttpPost("finished/datatable/teammember")]
        public async Task<IActionResult> FinishedTasksByTeamMemberDatatable([FromBody] PaginationFilter filter, string teamMemberId)
        {
            if (service.FinishedTasksByTeamMemberDatatable(filter, teamMemberId).Result.Data == null
               || !service.FinishedTasksByTeamMemberDatatable(filter, teamMemberId).Result.Data.Any())
            {
                return ResponseHelper<List<TaskDTO>>.UnexistingResource(new List<TaskDTO>());
            }

            return new ObjectResult(await service.FinishedTasksByTeamMemberDatatable(filter, teamMemberId));
        }

        #endregion

        #region POST

        /// <summary>
        /// Create Task
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST task/create
        ///     {
        ///         "title": "Create an API for the Project Management Software",
        ///         "status": "Unassigned",
        ///         "startDate": "2008-11-11 11:12:01",
        ///         "endDate": "2008-12-11 11:12:01",
        ///         "projectId": "dhsjl1l1k*shh/292==",
        ///         "teamMemberId": "5435==hhs1*shh/124",
        ///         "Role": "Back End"
        ///     }
        /// 
        /// *Mandatory Fields: Title, Status, Start Date, End Date, ProjectId
        /// </remarks>
        /// <param name="task">Task's data</param>
        /// <returns>ID of the newly created task</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="400">Indicates that the input was incorrect</response>
        [Authenticate]
        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody] CreateTaskDTO task)
        {
            if (string.IsNullOrEmpty(task.Title)
                || string.IsNullOrEmpty(task.Status)
                || task.StartDate == null
                || task.EndDate == null
                || string.IsNullOrEmpty(task.ProjectId)
                )
            {
                return ResponseHelper<CreateTaskDTO>.IncorrectInput(task);
            }

            if (projectService.GetProject(task.ProjectId).Result == null)
            {
                return ResponseHelper<ProjectDTO>.UnexistingResource(new ProjectDTO());
            }

            return ResponseHelper<string>.Success(await service.Create(task));
        }

        #endregion

        #region PUT

        /// <summary>
        /// Update task
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST task/update
        ///     {
        ///         "Id": 24,
        ///         "title": "ReactJS Landing Page",
        ///         "status": "Assigned",
        ///         "StartDate": "2008-11-11 11:12:01",
        ///         "EndDate": "2008-12-11 11:12:01",
        ///         "TeamMemberId": 31
        ///     }
        ///     
        /// *Mandatory Fields: Id, Title, Status, StartDate, EndDate
        /// </remarks>
        /// <param name="task">Updated task data</param>
        /// <returns>True if the request has succeeded, otherwise false</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource does not exist</response>
        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody] UpdateTaskDTO task)
        {
            if (service.GetTask(task.Id).Result == null)
            {
                return ResponseHelper<TaskDTO>.UnexistingResource(new TaskDTO());
            }

            if (string.IsNullOrEmpty(task.Title) 
                || string.IsNullOrEmpty(task.Status) 
                || task.StartDate == null 
                || task.EndDate == null
                )
            {
                return ResponseHelper<UpdateTaskDTO>.IncorrectInput(task);
            }

            return ResponseHelper<bool>.Success(await service.Update(task));
        }

        #endregion
    }
}
