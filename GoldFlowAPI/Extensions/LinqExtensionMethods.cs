﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace GoldFlowAPI.Extensions
{
    public static class LinqExtensionMethods
    {
        public static IQueryable<TEntity> CustomOrderBy<TEntity>(this IQueryable<TEntity> source, 
                                                           string orderByProperty, 
                                                           string direction)
        {
            string command = direction.ToLower() == "desc" ? "OrderByDescending" : "OrderBy";

            var type = typeof(TEntity);

            var property = type.GetProperty(orderByProperty);

            var parameter = Expression.Parameter(type, "p");

            var propertyAccess = Expression.MakeMemberAccess(parameter, property);

            var orderByExpression = Expression.Lambda(propertyAccess, parameter);

            var resultExpression = Expression.Call(typeof(Queryable), 
                                                   command, 
                                                   new Type[] { type, property.PropertyType },
                                                   source.Expression, 
                                                   Expression.Quote(orderByExpression));

            return source.Provider.CreateQuery<TEntity>(resultExpression);
        }
    }
}
