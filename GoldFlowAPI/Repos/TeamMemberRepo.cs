﻿using DTOs;
using GoldFlowAPI.Extensions;
using GoldFlowAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoldFlowAPI.Repos
{
    public class TeamMemberRepo : ITeamMemberRepo
    {
        #region GET

        public async Task<List<TeamMember>> GetAll()
        {
            await using var db = new GoldFlowContext();
            return db.TeamMembers.ToList();
        }

        #endregion

        #region POST

        public async Task<string> Create(TeamMember teamMember)
        {
            await using var db = new GoldFlowContext();
            db.TeamMembers.Add(teamMember);
            db.SaveChanges();

            return teamMember.Id.ToString();
        }

        #endregion

        #region COUNT

        public async Task<int> CountTeamMembers()
        {
            await using var db = new GoldFlowContext();
            return db.TeamMembers.Count();
        }

        #endregion

        #region Datatables

        public async Task<List<TeamMember>> TeamMembersDatatable(PaginationFilter filter)
        {
            await using var db = new GoldFlowContext();

            return db.TeamMembers.CustomOrderBy(filter.SortColumn, filter.SortDirection)
                                 .Skip((filter.PageNumber - 1) * filter.PageSize)
                                 .Take(filter.PageSize)
                                 .ToList();
        }

        #endregion
    }
}
