﻿using DTOs;
using GoldFlowAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoldFlowAPI.Repos
{
    public class CommentRepo : ICommentRepo
    {
        #region COUNT

        public async Task<int> CountComments()
        {
            await using var db = new GoldFlowContext();
            return db.Comments.Count();
        }

        #endregion

        #region GET

        public async Task<List<Comment>> GetAll()
        {
            await using var db = new GoldFlowContext();
            return db.Comments.ToList();
        }

        #endregion

        #region Datatables

        public async Task<List<Comment>> CommentsDatatable(PaginationFilter filter)
        {
            await using var db = new GoldFlowContext();

            return db.Comments.Skip((filter.PageNumber - 1) * filter.PageSize)
                              .Take(filter.PageSize)
                              .ToList();
        }

        #endregion

        #region POST

        public async Task<string> Create(Comment comment)
        {
            await using var db = new GoldFlowContext();
            db.Comments.Add(comment);
            db.SaveChanges();

            return comment.Id.ToString();
        }

        #endregion

        #region DELETE

        public async Task<bool> DeleteComment(int id)
        {
            await using var db = new GoldFlowContext();
            Comment comment = db.Comments.Where(c => c.Id == id).FirstOrDefault();
            db.Comments.Remove(comment);
            db.SaveChanges();

            return true;
        }

        #endregion
    }
}
