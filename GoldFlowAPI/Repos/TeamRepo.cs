﻿using DTOs;
using GoldFlowAPI.Extensions;
using GoldFlowAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoldFlowAPI.Repos
{
    public class TeamRepo : ITeamRepo
    {
        #region GET

        public async Task<List<Team>> GetAll()
        {
            await using var db = new GoldFlowContext();
            return db.Teams.ToList();
        }

        #endregion

        #region POST

        public async Task<string> Create(Team team)
        {
            await using var db = new GoldFlowContext();
            db.Teams.Add(team);
            db.SaveChanges();

            return team.Name;
        }

        #endregion

        #region COUNT

        public async Task<int> CountTeams()
        {
            await using var db = new GoldFlowContext();
            return db.Teams.Count();
        }

        #endregion

        #region Datatables

        public async Task<List<Team>> TeamsDatatable(PaginationFilter filter)
        {
            await using var db = new GoldFlowContext();

            return db.Teams.CustomOrderBy(filter.SortColumn, filter.SortDirection)
                           .Skip((filter.PageNumber - 1) * filter.PageSize)
                           .Take(filter.PageSize)
                           .ToList();
        }

        #endregion
    }
}
