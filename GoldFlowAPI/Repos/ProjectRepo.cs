﻿using DTOs;
using GoldFlowAPI.Extensions;
using GoldFlowAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoldFlowAPI.Repos
{
    public class ProjectRepo : IProjectRepo
    {
        #region COUNT

        public async Task<int> CountProjects()
        {
            await using var db = new GoldFlowContext();
            return db.Projects.Count();
        }

        public async Task<int> CountUnassignedProjects()
        {
            await using var db = new GoldFlowContext();
            return db.Projects.Where(p => p.Status == "Unassigned").Count();
        }

        #endregion

        #region GET

        public async Task<List<Project>> GetAll()
        {
            await using var db = new GoldFlowContext();
            return db.Projects.ToList();
        }

        public async Task<Project> GetProject(int id)
        {
            await using var db = new GoldFlowContext();
            return db.Projects.Where(p => p.Id == id).FirstOrDefault();
        }

        public async Task<List<Project>> GetUnassigned()
        {
            await using var db = new GoldFlowContext();
            return db.Projects.Where(p => p.Status == "Unassigned").ToList();
        }

        #endregion

        #region Datatables

        public async Task<List<Project>> ProjectsDatatable(PaginationFilter filter)
        {
            await using var db = new GoldFlowContext();

            return db.Projects.CustomOrderBy(filter.SortColumn, filter.SortDirection)
                              .Skip((filter.PageNumber - 1) * filter.PageSize)
                              .Take(filter.PageSize)
                              .ToList();
        }

        public async Task<List<Project>> UnassignedProjectsDatatable(PaginationFilter filter)
        {
            await using var db = new GoldFlowContext();

            return db.Projects.Where(p => p.Status == "Unassigned")
                              .CustomOrderBy(filter.SortColumn, filter.SortDirection)
                              .Skip((filter.PageNumber - 1) * filter.PageSize)
                              .Take(filter.PageSize)
                              .ToList();
        }

        #endregion

        #region POST

        public async Task<string> Create(Project project)
        {
            await using var db = new GoldFlowContext();
            db.Projects.Add(project);
            db.SaveChanges();

            return project.Id.ToString();
        }

        #endregion

        #region DELETE

        public async Task<bool> DeleteProject(int id)
        {
            await using var db = new GoldFlowContext();

            Project project = db.Projects.Where(p => p.Id == id).FirstOrDefault();
            db.Projects.Remove(project);
            db.SaveChanges();

            return true;
        }

        #endregion
    }
}
