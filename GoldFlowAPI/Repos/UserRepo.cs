﻿using DTOs;
using GoldFlowAPI.Extensions;
using GoldFlowAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoldFlowAPI.Repos
{
    public class UserRepo : IUserRepo
    {
        #region COUNT

        public async Task<int> CountUsers()
        {
            await using var db = new GoldFlowContext();
            return db.Users.Count();
        }

        #endregion

        #region POST

        public async Task<string> Create(User user)
        {
            await using var db = new GoldFlowContext();
            db.Users.Add(user);
            db.SaveChanges();

            return user.Id.ToString();
        }

        #endregion

        #region DELETE

        public async Task<bool> DeleteUser(int id)
        {
            await using var db = new GoldFlowContext();
            db.Users.Remove(await GetUser(id));
            db.SaveChanges();

            return true;
        }

        #endregion

        #region GET

        public async Task<List<User>> GetAll()
        {
            await using var db = new GoldFlowContext();
            return db.Users.ToList();
        }

        public async Task<User> GetUser(int id)
        {
            await using var db = new GoldFlowContext();
            return db.Users.Where(u => u.Id == id).FirstOrDefault();
        }

        #endregion

        #region Datatables

        public async Task<List<User>> UsersDatatable(PaginationFilter filter)
        {
            await using var db = new GoldFlowContext();
            return db.Users.CustomOrderBy(filter.SortColumn, filter.SortDirection)
                           .Skip((filter.PageNumber - 1) * filter.PageSize)
                           .Take(filter.PageSize)
                           .ToList();
        }

        #endregion

        #region PUT

        public async Task<bool> Update(User user)
        {
            await using var db = new GoldFlowContext();

            User userUpdate = db.Users.Where(u => u.Id == user.Id).FirstOrDefault();

            userUpdate.Category = user.Category;
            userUpdate.FirstName = user.FirstName;
            userUpdate.LastName = user.LastName;
            userUpdate.Email = user.Email;
            userUpdate.Password = user.Password;

            db.SaveChanges();

            return true;
        }

        #endregion

        #region LOGIN

        public async Task<User> Login(string email, string password)
        {
            await using var db = new GoldFlowContext();
            return db.Users.Where(u => u.Email == email && u.Password == password).FirstOrDefault();
        }

        #endregion
    }
}
