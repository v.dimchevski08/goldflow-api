﻿using DTOs;
using GoldFlowAPI.Extensions;
using GoldFlowAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoldFlowAPI.Repos
{
    public class TaskRepo : ITaskRepo
    {
        #region COUNT

        public async Task<int> TasksCountByTeamMember(int teamMemberId)
        {
            await using var db = new GoldFlowContext();
            return db.Tasks.Where(t => t.TeamMemberId == teamMemberId).Count();
        }

        public async Task<int> TasksCountByProject(int projectId)
        {
            await using var db = new GoldFlowContext();
            return db.Tasks.Where(t => t.ProjectId == projectId).Count();
        }

        public async Task<int> UnassignedTasksCount()
        {
            await using var db = new GoldFlowContext();
            return db.Tasks.Where(t => t.Status == "Unassigned").Count();
        }

        public async Task<int> FinishedTasksCountByTeamMember(int teamMemberId)
        {
            await using var db = new GoldFlowContext();
            return db.Tasks.Where(t => t.TeamMemberId == teamMemberId && t.Status == "Finished").Count();
        }

        #endregion

        #region GET

        public async Task<Models.Task> GetTask(int id)
        {
            await using var db = new GoldFlowContext();
            return db.Tasks.Where(t => t.Id == id).FirstOrDefault();
        }

        public async Task<List<Models.Task>> GetUnassignedTasks()
        {
            await using var db = new GoldFlowContext();
            return db.Tasks.Where(t => t.Status == "Unassigned").ToList();
        }

        public async Task<List<Models.Task>> GetTasksByTeamMember(int teamMemberId)
        {
            await using var db = new GoldFlowContext();
            return db.Tasks.Where(t => t.TeamMemberId == teamMemberId).ToList();
        }

        public async Task<List<Models.Task>> GetFinishedTasksByTeamMember(int teamMemberId)
        {
            await using var db = new GoldFlowContext();
            return db.Tasks.Where(t => t.TeamMemberId == teamMemberId && t.Status == "Finished").ToList();
        }

        public async Task<List<Models.Task>> GetTasksByProject(int projectId)
        {
            await using var db = new GoldFlowContext();
            return db.Tasks.Where(t => t.ProjectId == projectId).ToList();
        }

        #endregion

        #region Datatables

        public async Task<List<Models.Task>> TasksByProjectDatatable(PaginationFilter filter, int projectId)
        {
            await using var db = new GoldFlowContext();

            return db.Tasks.Where(t => t.ProjectId == projectId)
                           .OrderBy(t => t.EndDate.Hour - t.StartDate.Hour)
                           .CustomOrderBy(filter.SortColumn, filter.SortDirection)
                           .Skip((filter.PageNumber - 1) * filter.PageSize)
                           .Take(filter.PageSize)
                           .ToList();
        }

        public async Task<List<Models.Task>> UnassignedTasksDatatable(PaginationFilter filter)
        {
            await using var db = new GoldFlowContext();

            return db.Tasks.Where(t => t.Status == "Unassigned")
                           .CustomOrderBy(filter.SortColumn, filter.SortDirection)
                           .Skip((filter.PageNumber - 1) * filter.PageSize)
                           .Take(filter.PageSize)
                           .ToList();
        }

        public async Task<List<Models.Task>> TasksByTeamMemberDatatable(PaginationFilter filter, int teamMemberId)
        {
            await using var db = new GoldFlowContext();

            return db.Tasks.Where(t => t.TeamMemberId == teamMemberId)
                           .CustomOrderBy(filter.SortColumn, filter.SortDirection)
                           .Skip((filter.PageNumber - 1) * filter.PageSize)
                           .Take(filter.PageSize)
                           .ToList();
        }

        public async Task<List<Models.Task>> FinishedTasksByTeamMemberDatatable(PaginationFilter filter, int teamMemberId)
        {
            await using var db = new GoldFlowContext();

            return db.Tasks.Where(t => t.TeamMemberId == teamMemberId && t.Status == "Finished")
                           .CustomOrderBy(filter.SortColumn, filter.SortDirection)
                           .Skip((filter.PageNumber - 1) * filter.PageSize)
                           .Take(filter.PageSize)
                           .ToList();
        }

        #endregion

        #region POST

        public async Task<string> Create(Models.Task task)
        {
            await using var db = new GoldFlowContext();
            db.Tasks.Add(task);
            db.SaveChanges();

            return task.Id.ToString();
        }

        #endregion

        #region PUT

        public async Task<bool> Update(Models.Task task)
        {
            await using var db = new GoldFlowContext();

            Models.Task taskUpdate = db.Tasks.Where(t => t.Id == task.Id).FirstOrDefault();

            taskUpdate.Title = task.Title;
            taskUpdate.Status = task.Status;
            taskUpdate.StartDate = task.StartDate;
            taskUpdate.EndDate = task.EndDate;
            taskUpdate.TeamMemberId = task.TeamMemberId;

            db.SaveChanges();

            return true;
        }

        #endregion
    }
}
