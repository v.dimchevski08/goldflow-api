﻿using DTOs;
using GoldFlowAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoldFlowAPI.Repos
{
    public interface ITeamRepo
    {
        Task<List<Team>> GetAll();
        Task<string> Create(Team team);
        Task<int> CountTeams();
        Task<List<Team>> TeamsDatatable(PaginationFilter filter);
    }
}
