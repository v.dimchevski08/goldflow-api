﻿using DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoldFlowAPI.Repos
{
    public interface ITaskRepo
    {
        Task<Models.Task> GetTask(int id);
        Task<List<Models.Task>> GetUnassignedTasks();
        Task<string> Create(Models.Task task);
        Task<bool> Update(Models.Task task);
        Task<int> TasksCountByTeamMember(int teamMemberId);
        Task<int> FinishedTasksCountByTeamMember(int teamMemberId);
        Task<int> TasksCountByProject(int projectId);
        Task<int> UnassignedTasksCount();
        Task<List<Models.Task>> TasksByProjectDatatable(PaginationFilter filter, int projectId);
        Task<List<Models.Task>> UnassignedTasksDatatable(PaginationFilter filter);
        Task<List<Models.Task>> GetTasksByTeamMember(int teamMemberId);
        Task<List<Models.Task>> TasksByTeamMemberDatatable(PaginationFilter filter, int teamMemberId);
        Task<List<Models.Task>> GetFinishedTasksByTeamMember(int teamMemberId);
        Task<List<Models.Task>> GetTasksByProject(int projectId);
        Task<List<Models.Task>> FinishedTasksByTeamMemberDatatable(PaginationFilter filter, int teamMemberId);
    }
}
