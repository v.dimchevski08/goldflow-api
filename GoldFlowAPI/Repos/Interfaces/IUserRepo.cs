﻿using DTOs;
using GoldFlowAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoldFlowAPI.Repos
{
    public interface IUserRepo
    {
        Task<List<User>> GetAll();
        Task<List<User>> UsersDatatable(PaginationFilter filter);
        Task<int> CountUsers();
        Task<User> GetUser(int id);
        Task<string> Create(User user);
        Task<bool> Update(User user);
        Task<bool> DeleteUser(int id);
        Task<User> Login(string email, string password);
    }
}
