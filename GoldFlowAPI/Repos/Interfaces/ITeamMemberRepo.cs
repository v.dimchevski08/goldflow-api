﻿using DTOs;
using GoldFlowAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoldFlowAPI.Repos
{
    public interface ITeamMemberRepo
    {
        Task<List<TeamMember>> GetAll();
        Task<string> Create(TeamMember teamMember);
        Task<int> CountTeamMembers();
        Task<List<TeamMember>> TeamMembersDatatable(PaginationFilter filter);
    }
}
