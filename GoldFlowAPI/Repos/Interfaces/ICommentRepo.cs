﻿using DTOs;
using GoldFlowAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoldFlowAPI.Repos
{
    public interface ICommentRepo
    {
        Task<List<Comment>> GetAll();
        Task<List<Comment>> CommentsDatatable(PaginationFilter filter);
        Task<int> CountComments();
        Task<string> Create(Comment comment);
        Task<bool> DeleteComment(int id);
    }
}
