﻿using DTOs;
using GoldFlowAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoldFlowAPI.Repos
{
    public interface IProjectRepo
    {
        Task<Project> GetProject(int id);
        Task<List<Project>> GetAll();
        Task<List<Project>> GetUnassigned();
        Task<string> Create(Project project);
        Task<bool> DeleteProject(int id);
        Task<int> CountProjects();
        Task<int> CountUnassignedProjects();
        Task<List<Project>> ProjectsDatatable(PaginationFilter filter);
        Task<List<Project>> UnassignedProjectsDatatable(PaginationFilter filter);
    }
}
