﻿using AutoMapper;
using DTOs;
using GoldFlowAPI.Models;
using GoldFlowAPI.Utilities;

namespace GoldFlowAPI.Mappings
{
    public class UserMapping : Profile
    {
        public UserMapping()
        {
            CreateMap<User, UserDTO>()
                .ForMember(d => d.Id, config => config.MapFrom(s => EncryptionHelper.Encrypt(s.Id.ToString())));

            CreateMap<CreateUserDTO, User>()
                .ForMember(d => d.Password, config => config.MapFrom(s => EncryptionHelper.Encrypt(s.Password)));

            CreateMap<UpdateUserDTO, User>()
                .ForMember(d => d.Id, config => config.MapFrom(s => EncryptionHelper.Decrypt(s.Id)))
                .ForMember(d => d.Password, config => config.MapFrom(s => EncryptionHelper.Encrypt(s.Password)));
        }
    }
}
