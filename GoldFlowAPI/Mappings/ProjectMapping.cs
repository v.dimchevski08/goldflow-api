﻿using AutoMapper;
using DTOs;
using GoldFlowAPI.Models;
using GoldFlowAPI.Utilities;

namespace GoldFlowAPI.Mappings
{
    public class ProjectMapping : Profile
    {
        public ProjectMapping()
        {
            CreateMap<Project, ProjectDTO>()
                .ForMember(d => d.Id, config => config.MapFrom(s => EncryptionHelper.Encrypt(s.Id.ToString())))
                .ForMember(d => d.ManagerId, config => config.MapFrom(s => EncryptionHelper.Encrypt(s.ManagerId.ToString())));

            CreateMap<CreateProjectDTO, Project>()
                .ForMember(d => d.ManagerId, config => config.MapFrom(s => EncryptionHelper.Decrypt(s.ManagerId)));
        }
    }
}
