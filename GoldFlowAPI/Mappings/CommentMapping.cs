﻿using AutoMapper;
using DTOs;
using GoldFlowAPI.Models;
using GoldFlowAPI.Utilities;

namespace GoldFlowAPI.Mappings
{
    public class CommentMapping : Profile
    {
        public CommentMapping()
        {
            CreateMap<Comment, CommentDTO>()
                .ForMember(d => d.Id, config => config.MapFrom(s => EncryptionHelper.Encrypt(s.Id.ToString())))
                .ForMember(d => d.TaskId, config => config.MapFrom(s => EncryptionHelper.Encrypt(s.TaskId.ToString())));

            CreateMap<CreateCommentDTO, Comment>()
                .ForMember(d => d.TaskId, config => config.MapFrom(s => EncryptionHelper.Decrypt(s.TaskId)));
        }
    }
}
