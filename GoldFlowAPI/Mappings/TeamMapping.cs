﻿using AutoMapper;
using DTOs;
using GoldFlowAPI.Models;
using GoldFlowAPI.Utilities;

namespace GoldFlowAPI.Mappings
{
    public class TeamMapping : Profile
    {
        public TeamMapping()
        {
            CreateMap<Team, TeamDTO>()
                .ForMember(d => d.Id, config => config.MapFrom(s => EncryptionHelper.Encrypt(s.Id.ToString())))
                .ForMember(d => d.ProjectId, config => config.MapFrom(s => EncryptionHelper.Encrypt(s.ProjectId.ToString())));

            CreateMap<CreateTeamDTO, Team>()
                .ForMember(d => d.ProjectId, config => config.MapFrom(s => EncryptionHelper.Decrypt(s.ProjectId)));
        }
    }
}
