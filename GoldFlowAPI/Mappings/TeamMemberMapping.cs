﻿using AutoMapper;
using DTOs;
using GoldFlowAPI.Models;
using GoldFlowAPI.Utilities;

namespace GoldFlowAPI.Mappings
{
    public class TeamMemberMapping : Profile
    {
        public TeamMemberMapping()
        {
            CreateMap<TeamMember, TeamMemberDTO>()
                .ForMember(d => d.Id, config => config.MapFrom(s => EncryptionHelper.Encrypt(s.Id.ToString())))
                .ForMember(d => d.TeamId, config => config.MapFrom(s => EncryptionHelper.Encrypt(s.TeamId.ToString())))
                .ForMember(d => d.UserId, config => config.MapFrom(s => EncryptionHelper.Encrypt(s.UserId.ToString())));

            CreateMap<CreateTeamMemberDTO, TeamMember>()
                .ForMember(d => d.UserId, config => config.MapFrom(s => EncryptionHelper.Decrypt(s.UserId)))
                .ForMember(d => d.TeamId, config => config.MapFrom(s => EncryptionHelper.Decrypt(s.TeamId)));
        }
    }
}
