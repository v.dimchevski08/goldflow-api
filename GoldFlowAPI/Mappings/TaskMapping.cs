﻿using AutoMapper;
using DTOs;
using GoldFlowAPI.Utilities;

namespace GoldFlowAPI.Mappings
{
    public class TaskMapping : Profile
    {
        public TaskMapping()
        {
            CreateMap<Models.Task, TaskDTO>()
                .ForMember(d => d.Id, config => config.MapFrom(s => EncryptionHelper.Encrypt(s.Id.ToString())))
                .ForMember(d => d.ProjectId, config => config.MapFrom(s => EncryptionHelper.Encrypt(s.ProjectId.ToString())))
                .ForMember(d => d.TeamMemberId, config => config.MapFrom(s => EncryptionHelper.Encrypt(s.TeamMemberId.ToString())));

            CreateMap<CreateTaskDTO, Models.Task>()
                .ForMember(d => d.ProjectId, config => config.MapFrom(s => EncryptionHelper.Decrypt(s.ProjectId)))
                .ForMember(d => d.TeamMemberId, config => config.MapFrom(s => EncryptionHelper.Decrypt(s.TeamMemberId)));

            CreateMap<UpdateTaskDTO, Models.Task>()
                .ForMember(d => d.Id, config => config.MapFrom(s => EncryptionHelper.Decrypt(s.Id)))
                .ForMember(d => d.TeamMemberId, config => config.MapFrom(s => EncryptionHelper.Decrypt(s.TeamMemberId)));
        }
    }
}
