using AutoMapper;
using GoldFlowAPI.Middlewares;
using GoldFlowAPI.Repos;
using GoldFlowAPI.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace GoldFlowAPI
{
    public class Startup
    {
        public static string TokenKey { get; private set; }
        public static string Issuer { get; private set; }
        public static string Audience { get; private set; }
        public static string UserId { get; set; }
        public static string Email { get; private set; }
        public static string Password { get; private set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            TokenKey = Configuration["AppSettings:TokenKey"];
            Issuer = Configuration["AppSettings:Issuer"];
            Audience = Configuration["AppSettings:Audience"];
            Email = Configuration["EmailCredentials:Email"];
            Password = Configuration["EmailCredentials:Password"];

            services.AddControllers();

            services.AddTransient<IUserRepo, UserRepo>();
            services.AddTransient<ITaskRepo, TaskRepo>();
            services.AddTransient<ITeamRepo, TeamRepo>();
            services.AddTransient<IProjectRepo, ProjectRepo>();
            services.AddTransient<ICommentRepo, CommentRepo>();
            services.AddTransient<ITeamMemberRepo, TeamMemberRepo>();

            services.AddTransient<IUserService, UserService>();
            services.AddTransient<ITaskService, TaskService>();
            services.AddTransient<ITeamService, TeamService>();
            services.AddTransient<IProjectService, ProjectService>();
            services.AddTransient<ICommentService, CommentService>();
            services.AddTransient<ITeamMemberService, TeamMemberService>();

            services.AddTransient<IEmailSender, EmailSender>();

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddSwaggerGen(s =>
            {
                s.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "GoldFlow API",
                    Description = "API for GoldFlow - Project Management Software",
                    Contact = new OpenApiContact()
                    {
                        Name = "Viktor Dimchevski",
                        Email = "v.dimchevski08@gmail.com"
                    },
                    License = new OpenApiLicense()
                    {
                        Name = "Free To Use"
                    }
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                s.IncludeXmlComments(xmlPath);

                s.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme 
                { 
                    Description = "JWT Authorization Header using the Bearer Scheme.\n\nEnter your Token to Authorize!",
                    In = ParameterLocation.Header,
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                s.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "Bearer",
                            Name = "Authorization",
                            In = ParameterLocation.Header
                        },
                        new List<string>()
                    }
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory logger)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<ExceptionHandlingMiddleware>();

            logger.AddFile("Logs/error-log-{Date}.txt");

            app.UseSwagger();

            app.UseSwaggerUI(s =>
            {
                s.SwaggerEndpoint("v1/swagger.json", "GoldFlow API");
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
