﻿using AutoMapper;
using DTOs;
using GoldFlowAPI.Models;
using GoldFlowAPI.Repos;
using GoldFlowAPI.Utilities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoldFlowAPI.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepo repo;
        private readonly IMapper mapper;

        public UserService(IUserRepo _repo, IMapper _mapper)
        {
            repo = _repo;
            mapper = _mapper;
        }

        #region GET

        public async Task<UserDTO> GetUser(string id)
        {
            return mapper.Map<UserDTO>(await repo.GetUser(int.Parse(EncryptionHelper.Decrypt(id))));
        }

        public async Task<List<UserDTO>> GetAll()
        {
            return mapper.Map<List<UserDTO>>(await repo.GetAll());
        }

        #endregion

        #region Datatables

        public async Task<PagedResponse<List<UserDTO>>> UsersDatatable(PaginationFilter filter)
        {
            List<string> availableSortColumns = new List<string>() { "Category" };

            filter.SortColumn = availableSortColumns.Contains(filter.SortColumn) ? filter.SortColumn : "Category";

            int usersCount = await repo.CountUsers();

            #region Response
            PagedResponse<List<UserDTO>> response = new PagedResponse<List<UserDTO>>()
            {
                TotalRecords = usersCount,
                PageSize = filter.PageSize,
                PageNumber = filter.PageNumber,
                TotalPages = (int)Math.Ceiling((decimal)usersCount / filter.PageSize),
                Data = mapper.Map<List<UserDTO>>(await repo.UsersDatatable(filter)),
                Message = "Success",
                StatusCode = 200,
                IsError = false
            };
            #endregion

            return response;
        }

        #endregion

        #region POST

        public async Task<string> Create(CreateUserDTO user)
        {
            return EncryptionHelper.Encrypt(await repo.Create(mapper.Map<User>(user)));
        }

        #endregion

        #region PUT

        public async Task<bool> Update(UpdateUserDTO user)
        {
            return await repo.Update(mapper.Map<User>(user));
        }

        #endregion

        #region DELETE

        public async Task<bool> DeleteUser(string id)
        {
            return await repo.DeleteUser(int.Parse(EncryptionHelper.Decrypt(id)));
        }

        #endregion

        #region LOGIN

        public async Task<LoginResponseDTO> Login(LoginRequestDTO request)
        {
            User user = await repo.Login(request.Email, request.Password);

            if (user != null)
            {
                return new LoginResponseDTO()
                {
                    UserDetails = mapper.Map<UserDTO>(user),
                    LoginToken = AuthenticationHelper.GenerateToken(user)
                };
            }

            return new LoginResponseDTO();
        }

        #endregion
    }
}
