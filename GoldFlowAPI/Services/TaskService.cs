﻿using AutoMapper;
using DTOs;
using GoldFlowAPI.Repos;
using GoldFlowAPI.Utilities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoldFlowAPI.Services
{
    public class TaskService : ITaskService
    {
        private readonly IMapper mapper;
        private readonly ITaskRepo repo;

        public TaskService(IMapper _mapper, ITaskRepo _repo)
        {
            mapper = _mapper;
            repo = _repo;
        }

        #region GET

        public async Task<TaskDTO> GetTask(string id)
        {
            return mapper.Map<TaskDTO>(await repo.GetTask(int.Parse(EncryptionHelper.Decrypt(id))));
        }

        public async Task<List<TaskDTO>> GetUnassignedTasks()
        {
            return mapper.Map<List<TaskDTO>>(await repo.GetUnassignedTasks());
        }

        public async Task<List<TaskDTO>> GetTasksByTeamMember(string teamMemberId)
        {
            return mapper.Map<List<TaskDTO>>(await repo.GetTasksByTeamMember(int.Parse(EncryptionHelper.Decrypt(teamMemberId))));
        }

        public async Task<List<TaskDTO>> GetFinishedTasksByTeamMember(string teamMemberId)
        {
            return mapper.Map<List<TaskDTO>>(await repo.GetFinishedTasksByTeamMember(int.Parse(EncryptionHelper.Decrypt(teamMemberId))));
        }

        public async Task<List<TaskDTO>> GetTasksByProject(string projectId)
        {
            return mapper.Map<List<TaskDTO>>(await repo.GetTasksByProject(int.Parse(EncryptionHelper.Decrypt(projectId))));
        }

        #endregion

        #region Datatables

        public async Task<PagedResponse<List<TaskDTO>>> TasksByProjectDatatable(PaginationFilter filter, string projectId)
        {
            List<string> availableSortColumns = new List<string>() { "Status" };

            filter.SortColumn = availableSortColumns.Contains(filter.SortColumn) ? filter.SortColumn : "Status";

            int tasksByProjectCount = await repo.TasksCountByProject(int.Parse(EncryptionHelper.Decrypt(projectId)));

            #region Response
            PagedResponse<List<TaskDTO>> response = new PagedResponse<List<TaskDTO>>()
            {
                TotalRecords = tasksByProjectCount,
                PageSize = filter.PageSize,
                PageNumber = filter.PageNumber,
                TotalPages = (int)Math.Ceiling((decimal)tasksByProjectCount / filter.PageSize),
                Data = mapper.Map<List<TaskDTO>>(await repo.TasksByProjectDatatable(filter, int.Parse(EncryptionHelper.Decrypt(projectId)))),
                Message = "Success",
                StatusCode = 200,
                IsError = false
            };
            #endregion

            return response;
        }

        public async Task<PagedResponse<List<TaskDTO>>> UnassignedTasksDatatable(PaginationFilter filter)
        {
            List<string> availableSortColumns = new List<string>() { "Status" };

            filter.SortColumn = availableSortColumns.Contains(filter.SortColumn) ? filter.SortColumn : "Status";

            int unassignedTasksCount = await repo.UnassignedTasksCount();

            #region Response
            PagedResponse<List<TaskDTO>> response = new PagedResponse<List<TaskDTO>>()
            {
                TotalRecords = unassignedTasksCount,
                PageSize = filter.PageSize,
                PageNumber = filter.PageNumber,
                TotalPages = (int)Math.Ceiling((decimal)unassignedTasksCount / filter.PageSize),
                Data = mapper.Map<List<TaskDTO>>(await repo.UnassignedTasksDatatable(filter)),
                Message = "Success",
                StatusCode = 200,
                IsError = false
            };
            #endregion

            return response;
        }

        public async Task<PagedResponse<List<TaskDTO>>> TasksByTeamMemberDatatable(PaginationFilter filter, string teamMemberId)
        {
            List<string> availableSortColumns = new List<string>() { "Status", "TeamMemberId", "Role" };

            filter.SortColumn = availableSortColumns.Contains(filter.SortColumn) ? filter.SortColumn : "TeamMemberId";

            int tasksByTeamMemberCount = await repo.TasksCountByTeamMember(int.Parse(EncryptionHelper.Decrypt(teamMemberId)));

            #region Response
            PagedResponse<List<TaskDTO>> response = new PagedResponse<List<TaskDTO>>()
            {
                TotalRecords = tasksByTeamMemberCount,
                PageSize = filter.PageSize,
                PageNumber = filter.PageNumber,
                TotalPages = (int)Math.Ceiling((decimal)tasksByTeamMemberCount / filter.PageSize),
                Data = mapper.Map<List<TaskDTO>>(await repo.TasksByTeamMemberDatatable(filter, int.Parse(EncryptionHelper.Decrypt(teamMemberId)))),
                Message = "Success",
                StatusCode = 200,
                IsError = false
            };
            #endregion

            return response;
        }

        public async Task<PagedResponse<List<TaskDTO>>> FinishedTasksByTeamMemberDatatable(PaginationFilter filter, string teamMemberId)
        {
            List<string> availableSortColumns = new List<string>() { "TeamMemberId", "Role" };

            filter.SortColumn = availableSortColumns.Contains(filter.SortColumn) ? filter.SortColumn : "TeamMemberId";

            int finishedTasksByTeamMemberCount = await repo.FinishedTasksCountByTeamMember(int.Parse(EncryptionHelper.Decrypt(teamMemberId)));

            #region Response
            PagedResponse<List<TaskDTO>> response = new PagedResponse<List<TaskDTO>>()
            {
                TotalRecords = finishedTasksByTeamMemberCount,
                PageSize = filter.PageSize,
                PageNumber = filter.PageNumber,
                TotalPages = (int)Math.Ceiling((decimal)finishedTasksByTeamMemberCount / filter.PageSize),
                Data = mapper.Map<List<TaskDTO>>(await repo.FinishedTasksByTeamMemberDatatable(filter, int.Parse(EncryptionHelper.Decrypt(teamMemberId)))),
                Message = "Success",
                StatusCode = 200,
                IsError = false
            };
            #endregion

            return response;
        }

        #endregion

        #region POST
        public async Task<string> Create(CreateTaskDTO task)
        {
            return EncryptionHelper.Encrypt(await repo.Create(mapper.Map<Models.Task>(task)));
        }
        #endregion

        #region PUT
        public async Task<bool> Update(UpdateTaskDTO task)
        {
            return await repo.Update(mapper.Map<Models.Task>(task));
        }
        #endregion
    }
}
