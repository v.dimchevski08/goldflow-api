﻿using MailKit.Net.Smtp;
using MimeKit;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoldFlowAPI.Services
{
    public class EmailSender : IEmailSender
    {
        #region Sending Email

        public async Task<bool> SendEmail(string from, List<string> to, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress(from));

            foreach(string recipient in to)
            {
                emailMessage.To.Add(new MailboxAddress(recipient));
            }

            emailMessage.Subject = subject;

            emailMessage.Body = new TextPart() { Text = message };

            using (var client = new SmtpClient())
            {
                client.Connect("smtp.live.com", 587, false);
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                await client.AuthenticateAsync(Startup.Email, Startup.Password);
                await client.SendAsync(emailMessage);
                await client.DisconnectAsync(true);
            }

            return true;
        }

        #endregion
    }
}