﻿using AutoMapper;
using DTOs;
using GoldFlowAPI.Models;
using GoldFlowAPI.Repos;
using GoldFlowAPI.Utilities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoldFlowAPI.Services
{
    public class CommentService : ICommentService
    {
        private readonly IMapper mapper;
        private readonly ICommentRepo repo;

        public CommentService(IMapper _mapper, ICommentRepo _repo)
        {
            repo = _repo;
            mapper = _mapper;
        }

        #region GET

        public async Task<List<CommentDTO>> GetAll()
        {
            return mapper.Map<List<CommentDTO>>(await repo.GetAll());
        }

        #endregion

        #region Datatables

        public async Task<PagedResponse<List<CommentDTO>>> CommentsDatatable(PaginationFilter filter)
        {
            int commentsCount = await repo.CountComments();

            #region Response
            PagedResponse<List<CommentDTO>> response = new PagedResponse<List<CommentDTO>>()
            {
                TotalRecords = commentsCount,
                PageSize = filter.PageSize,
                PageNumber = filter.PageNumber,
                TotalPages = (int)Math.Ceiling((decimal)commentsCount / filter.PageSize),
                Data = mapper.Map<List<CommentDTO>>(await repo.CommentsDatatable(filter)),
                Message = "Success",
                StatusCode = 200,
                IsError = false
            };
            #endregion

            return response;
        }

        #endregion

        #region POST

        public async Task<string> Create(CreateCommentDTO comment)
        {
            return EncryptionHelper.Encrypt(await repo.Create(mapper.Map<Comment>(comment)));
        }

        #endregion

        #region DELETE

        public async Task<bool> DeleteComment(string id)
        {
            return await repo.DeleteComment(int.Parse(EncryptionHelper.Decrypt(id)));
        }

        #endregion
    }
}
