﻿using AutoMapper;
using DTOs;
using GoldFlowAPI.Models;
using GoldFlowAPI.Repos;
using GoldFlowAPI.Utilities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoldFlowAPI.Services
{
    public class TeamMemberService : ITeamMemberService
    {
        private readonly IMapper mapper;
        private readonly ITeamMemberRepo repo;

        public TeamMemberService(IMapper _mapper, ITeamMemberRepo _repo)
        {
            mapper = _mapper;
            repo = _repo;
        }

        #region GET

        public async Task<List<TeamMemberDTO>> GetAll()
        {
            return mapper.Map<List<TeamMemberDTO>>(await repo.GetAll());
        }

        #endregion

        #region POST

        public async Task<string> Create(CreateTeamMemberDTO teamMember)
        {
            return EncryptionHelper.Encrypt(await repo.Create(mapper.Map<TeamMember>(teamMember)));
        }

        #endregion

        #region Datatables

        public async Task<PagedResponse<List<TeamMemberDTO>>> TeamMembersDatatable(PaginationFilter filter)
        {
            List<string> availableSortColumns = new List<string>() { "Role", "TeamId", "UserId" };

            filter.SortColumn = availableSortColumns.Contains(filter.SortColumn) ? filter.SortColumn : "Role";

            int teamMembersCount = await repo.CountTeamMembers();

            #region Response

            PagedResponse<List<TeamMemberDTO>> response = new PagedResponse<List<TeamMemberDTO>>()
            {
                TotalRecords = teamMembersCount,
                PageSize = filter.PageSize,
                PageNumber = filter.PageNumber,
                TotalPages = (int)Math.Ceiling((decimal)teamMembersCount / filter.PageSize),
                Data = mapper.Map<List<TeamMemberDTO>>(await repo.TeamMembersDatatable(filter)),
                Message = "Success",
                StatusCode = 200,
                IsError = false
            };

            #endregion

            return response;
        }

        #endregion
    }
}
