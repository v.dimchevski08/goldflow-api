﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoldFlowAPI.Services
{
    public interface IEmailSender
    {
        Task<bool> SendEmail(string from, List<string> to, string subject, string message);
    }
}
