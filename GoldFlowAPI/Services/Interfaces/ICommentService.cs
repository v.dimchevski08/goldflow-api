﻿using DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoldFlowAPI.Services
{
    public interface ICommentService
    {
        Task<List<CommentDTO>> GetAll();
        Task<PagedResponse<List<CommentDTO>>> CommentsDatatable(PaginationFilter filter);
        Task<string> Create(CreateCommentDTO comment);
        Task<bool> DeleteComment(string id);
    }
}
