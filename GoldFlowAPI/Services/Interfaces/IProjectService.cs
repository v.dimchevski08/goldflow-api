﻿using DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoldFlowAPI.Services
{
    public interface IProjectService
    {
        Task<List<ProjectDTO>> GetAll();
        Task<ProjectDTO> GetProject(string id);
        Task<List<ProjectDTO>> GetUnassigned();
        Task<string> Create(CreateProjectDTO project);
        Task<bool> DeleteProject(string id);
        Task<PagedResponse<List<ProjectDTO>>> ProjectsDatatable(PaginationFilter filter);
        Task<PagedResponse<List<ProjectDTO>>> UnassignedProjectsDatatable(PaginationFilter filter);
    }
}
