﻿using DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoldFlowAPI.Services
{
    public interface ITaskService
    {
        Task<TaskDTO> GetTask(string id);
        Task<List<TaskDTO>> GetUnassignedTasks();
        Task<string> Create(CreateTaskDTO task);
        Task<bool> Update(UpdateTaskDTO task);
        Task<PagedResponse<List<TaskDTO>>> TasksByProjectDatatable(PaginationFilter filter, string projectId);
        Task<PagedResponse<List<TaskDTO>>> UnassignedTasksDatatable(PaginationFilter filter);
        Task<List<TaskDTO>> GetTasksByTeamMember(string teamMemberId);
        Task<PagedResponse<List<TaskDTO>>> TasksByTeamMemberDatatable(PaginationFilter filter, string teamMemberId);
        Task<List<TaskDTO>> GetFinishedTasksByTeamMember(string teamMemberId);
        Task<List<TaskDTO>> GetTasksByProject(string projectId);
        Task<PagedResponse<List<TaskDTO>>> FinishedTasksByTeamMemberDatatable(PaginationFilter filter, string teamMemberId);
    }
}
