﻿using DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoldFlowAPI.Services
{
    public interface IUserService
    {
        Task<string> Create(CreateUserDTO user);
        Task<UserDTO> GetUser(string id);
        Task<List<UserDTO>> GetAll();
        Task<PagedResponse<List<UserDTO>>> UsersDatatable(PaginationFilter filter);
        Task<bool> Update(UpdateUserDTO user);
        Task<bool> DeleteUser(string id);
        Task<LoginResponseDTO> Login(LoginRequestDTO request);
    }
}
