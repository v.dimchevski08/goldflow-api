﻿using DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoldFlowAPI.Services
{
    public interface ITeamService
    {
        Task<List<TeamDTO>> GetAll();
        Task<string> Create(CreateTeamDTO team);
        Task<PagedResponse<List<TeamDTO>>> TeamsDatatable(PaginationFilter filter);
    }
}
