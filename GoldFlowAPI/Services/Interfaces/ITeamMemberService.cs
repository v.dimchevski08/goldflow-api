﻿using DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoldFlowAPI.Services
{
    public interface ITeamMemberService
    {
        Task<List<TeamMemberDTO>> GetAll();
        Task<string> Create(CreateTeamMemberDTO teamMember);
        Task<PagedResponse<List<TeamMemberDTO>>> TeamMembersDatatable(PaginationFilter filter);
    }
}
