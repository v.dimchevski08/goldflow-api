﻿using AutoMapper;
using DTOs;
using GoldFlowAPI.Models;
using GoldFlowAPI.Repos;
using GoldFlowAPI.Utilities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoldFlowAPI.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IMapper mapper;
        private readonly IProjectRepo repo;

        public ProjectService(IMapper _mapper, IProjectRepo _repo)
        {
            repo = _repo;
            mapper = _mapper;
        }

        #region GET

        public async Task<List<ProjectDTO>> GetAll()
        {
            return mapper.Map<List<ProjectDTO>>(await repo.GetAll());
        }

        public async Task<ProjectDTO> GetProject(string id)
        {
            return mapper.Map<ProjectDTO>(await repo.GetProject(int.Parse(EncryptionHelper.Decrypt(id))));
        }

        public async Task<List<ProjectDTO>> GetUnassigned()
        {
            return mapper.Map<List<ProjectDTO>>(await repo.GetUnassigned());
        }

        #endregion

        #region Datatables

        public async Task<PagedResponse<List<ProjectDTO>>> ProjectsDatatable(PaginationFilter filter)
        {
            List<string> availableSortColumns = new List<string>() { "Status" };

            filter.SortColumn = availableSortColumns.Contains(filter.SortColumn) ? filter.SortColumn : "Status";

            int projectsCount = await repo.CountProjects();

            #region Response
            PagedResponse<List<ProjectDTO>> response = new PagedResponse<List<ProjectDTO>>()
            {
                TotalRecords = projectsCount,
                PageSize = filter.PageSize,
                PageNumber = filter.PageNumber,
                TotalPages = (int)Math.Ceiling((decimal)projectsCount / filter.PageSize),
                Data = mapper.Map<List<ProjectDTO>>(await repo.ProjectsDatatable(filter)),
                Message = "Success",
                StatusCode = 200,
                IsError = false
            };
            #endregion

            return response;
        }

        public async Task<PagedResponse<List<ProjectDTO>>> UnassignedProjectsDatatable(PaginationFilter filter)
        {
            List<string> availableSortColumns = new List<string>() { "Status" };

            filter.SortColumn = availableSortColumns.Contains(filter.SortColumn) ? filter.SortColumn : "Status";

            int unassignedProjectsCount = await repo.CountUnassignedProjects();

            #region Response
            PagedResponse<List<ProjectDTO>> response = new PagedResponse<List<ProjectDTO>>()
            {
                TotalRecords = unassignedProjectsCount,
                PageSize = filter.PageSize,
                PageNumber = filter.PageNumber,
                TotalPages = (int)Math.Ceiling((decimal)unassignedProjectsCount / filter.PageSize),
                Data = mapper.Map<List<ProjectDTO>>(await repo.UnassignedProjectsDatatable(filter)),
                Message = "Success",
                StatusCode = 200,
                IsError = false
            };
            #endregion

            return response;
        }

        #endregion

        #region POST

        public async Task<string> Create(CreateProjectDTO project)
        {
            return EncryptionHelper.Encrypt(await repo.Create(mapper.Map<Project>(project)));
        }

        #endregion

        #region DELETE

        public async Task<bool> DeleteProject(string id)
        {
            return await repo.DeleteProject(int.Parse(EncryptionHelper.Decrypt(id)));
        }

        #endregion
    }
}
