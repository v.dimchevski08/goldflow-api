﻿using AutoMapper;
using DTOs;
using GoldFlowAPI.Models;
using GoldFlowAPI.Repos;
using GoldFlowAPI.Utilities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoldFlowAPI.Services
{
    public class TeamService : ITeamService
    {
        private readonly IMapper mapper;
        private readonly ITeamRepo repo;

        public TeamService(IMapper _mapper, ITeamRepo _repo)
        {
            mapper = _mapper;
            repo = _repo;
        }

        #region GET

        public async Task<List<TeamDTO>> GetAll()
        {
            return mapper.Map<List<TeamDTO>>(await repo.GetAll());
        }

        #endregion

        #region POST

        public async Task<string> Create(CreateTeamDTO team)
        {
            return EncryptionHelper.Encrypt(await repo.Create(mapper.Map<Team>(team)));
        }

        #endregion

        #region Datatables

        public async Task<PagedResponse<List<TeamDTO>>> TeamsDatatable(PaginationFilter filter)
        {
            List<string> availableSortColumns = new List<string>() { "ProjectId" };

            filter.SortColumn = availableSortColumns.Contains(filter.SortColumn) ? filter.SortColumn : "Name";

            int teamsCount = await repo.CountTeams();

            #region Response

            PagedResponse<List<TeamDTO>> response = new PagedResponse<List<TeamDTO>>()
            {
                TotalRecords = teamsCount,
                PageSize = filter.PageSize,
                PageNumber = filter.PageNumber,
                TotalPages = (int)Math.Ceiling((decimal)teamsCount / filter.PageSize),
                Data = mapper.Map<List<TeamDTO>>(await repo.TeamsDatatable(filter)),
                Message = "Success",
                StatusCode = 200,
                IsError = false
            };

            #endregion

            return response;
        }

        #endregion
    }
}
