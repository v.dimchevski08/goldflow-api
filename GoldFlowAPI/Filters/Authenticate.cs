﻿using GoldFlowAPI.Utilities;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace GoldFlowAPI.Filters
{
    public class Authenticate : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var headers = context.HttpContext.Request.Headers;
            string token = headers["Authorization"];

            if (string.IsNullOrEmpty(token))
            {
                context.Result = ResponseHelper<string>.UnexistingResource(token);
                return;
            }

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();

            token = token.Replace("Bearer", string.Empty);

            JwtSecurityToken jwt = (JwtSecurityToken)tokenHandler.ReadToken(token);

            if(jwt == null)
            {
                context.Result = ResponseHelper<JwtSecurityToken>.UnexistingResource(jwt);
                return;
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Startup.TokenKey));

            TokenValidationParameters parameters = new TokenValidationParameters()
            {
                RequireExpirationTime = true,
                ValidateIssuer = false,
                ValidateAudience = false,
                IssuerSigningKey = key
            };

            SecurityToken securityToken;
            ClaimsIdentity claimsIdentity;

            try
            {
                ClaimsPrincipal claimsPrincipal = tokenHandler.ValidateToken(token, parameters, out securityToken);

                if(claimsPrincipal == null)
                {
                    context.Result = ResponseHelper<ClaimsPrincipal>.UnexistingResource(claimsPrincipal);
                    return;
                }

                claimsIdentity = (ClaimsIdentity)claimsPrincipal.Identity;
            }
            catch (NullReferenceException)
            {
                context.Result = ResponseHelper<JwtSecurityToken>.IncorrectInput(jwt);
                return;
            }
            catch (SecurityTokenExpiredException)
            {
                context.Result = ResponseHelper<string>.IncorrectInput("Token expired!");
                return;
            }

            Claim userId = claimsIdentity.FindFirst("UserId");

            if(userId != null)
            {
                Startup.UserId = userId.Value;
            }
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            return;
        }
    }
}
