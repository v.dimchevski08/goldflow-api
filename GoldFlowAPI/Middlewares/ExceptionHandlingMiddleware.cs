﻿using DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace GoldFlowAPI.Middlewares
{
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate next;
        private readonly ILogger<ExceptionHandlingMiddleware> logger;

        public ExceptionHandlingMiddleware(RequestDelegate _next, ILogger<ExceptionHandlingMiddleware> _logger)
        {
            next = _next;
            logger = _logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch(Exception ex)
            {
                await HandleError(context, ex);
            }
        }

        #region Error Handler
        private async Task HandleError(HttpContext context, Exception ex)
        {
            int statusCode = 500;
            string message = "Internal Server Error";

            #region Log Exception To File
            StackTrace stackTrace = new StackTrace(ex, true);
            StackFrame stackFrame = stackTrace.GetFrame(0);

            string fileName = stackFrame.GetFileName();
            string methodName = stackFrame.GetMethod().Name;
            int lineNumberFromFrame = stackFrame.GetFileLineNumber();
            string lineNumberFromEx = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
            string exceptionType = ex.GetType().ToString();

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(Environment.NewLine);
            stringBuilder.AppendLine(Environment.NewLine);
            stringBuilder.AppendLine("------------------------------------------------------------------------------");
            stringBuilder.AppendLine($"Error Message: {ex.Message}");
            stringBuilder.AppendLine($"Error Type: {exceptionType}");
            stringBuilder.AppendLine($"File: {fileName}");
            stringBuilder.AppendLine($"Method: {methodName}");
            stringBuilder.AppendLine($"Line: {lineNumberFromFrame}");
            stringBuilder.AppendLine($"Line: {lineNumberFromEx}");
            stringBuilder.AppendLine($"Date: {DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss")}");
            stringBuilder.AppendLine("------------------------------------------------------------------------------");
            stringBuilder.AppendLine(Environment.NewLine);

            logger.LogError(stringBuilder.ToString());
            #endregion

            #region Customize Info By Type
            Type type = ex.GetType();

            if (type == typeof(KeyNotFoundException))
            {
                message = "The resource is non-existing";
                statusCode = 404;
            }
            else if (type == typeof(ArgumentException))
            {
                message = "Incorrect input";
                statusCode = 400;
            }
            else if (type == typeof(SqlException))
            {
                message = "Database Error";
                statusCode = 500;
            }
            #endregion

            ErrorModel model = new ErrorModel()
            {
                Message = message,
                StatusCode = statusCode,
                OriginalErrorMessage = ex.Message
            };

            context.Response.ContentType = "application/json";

            await context.Response.WriteAsync(JsonConvert.SerializeObject(model));
        }
        #endregion
    }
}
