﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GoldFlowAPI.Models
{
    public partial class Project
    {
        public Project()
        {
            Tasks = new HashSet<Task>();
            Teams = new HashSet<Team>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int ManagerId { get; set; }

        public virtual User Manager { get; set; }
        public virtual ICollection<Task> Tasks { get; set; }
        public virtual ICollection<Team> Teams { get; set; }
    }
}
