﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GoldFlowAPI.Models
{
    public partial class TeamMember
    {
        public TeamMember()
        {
            Tasks = new HashSet<Task>();
        }

        public int Id { get; set; }
        public string Role { get; set; }
        public int UserId { get; set; }
        public int TeamId { get; set; }

        public virtual Team Team { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<Task> Tasks { get; set; }
    }
}
