﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GoldFlowAPI.Models
{
    public partial class User
    {
        public User()
        {
            Projects = new HashSet<Project>();
            TeamMembers = new HashSet<TeamMember>();
        }

        public int Id { get; set; }
        public string Category { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public virtual ICollection<Project> Projects { get; set; }
        public virtual ICollection<TeamMember> TeamMembers { get; set; }
    }
}
