﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GoldFlowAPI.Models
{
    public partial class Team
    {
        public Team()
        {
            TeamMembers = new HashSet<TeamMember>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int ProjectId { get; set; }

        public virtual Project Project { get; set; }
        public virtual ICollection<TeamMember> TeamMembers { get; set; }
    }
}
