﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GoldFlowAPI.Models
{
    public partial class Task
    {
        public Task()
        {
            Comments = new HashSet<Comment>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int ProjectId { get; set; }
        public int? TeamMemberId { get; set; }
        public string Role { get; set; }

        public virtual Project Project { get; set; }
        public virtual TeamMember TeamMember { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
