﻿namespace DTOs
{
    public class LoginResponseDTO
    {
        public UserDTO UserDetails { get; set; }
        public string LoginToken { get; set; }
    }
}
