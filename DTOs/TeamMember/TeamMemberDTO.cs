﻿namespace DTOs
{
    public class TeamMemberDTO
    {
        public string Id { get; set; }
        public string Role { get; set; }
        public string UserId { get; set; }
        public string TeamId { get; set; }
    }
}
