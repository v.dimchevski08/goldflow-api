﻿namespace DTOs
{
    public class CreateTeamMemberDTO
    {
        public string Role { get; set; }
        public string UserId { get; set; }
        public string TeamId { get; set; }
    }
}
