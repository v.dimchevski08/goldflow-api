﻿namespace DTOs
{
    public class Response<T>
    {
        public string Message { get; set; }
        public int StatusCode { get; set; }
        public bool IsError { get; set; }
        public T Data { get; set; }

        public Response()
        {

        }

        public Response(T data, int statusCode = 200, bool isError = false, string message = "Success")
        {
            Data = data;
            StatusCode = statusCode;
            IsError = isError;
            Message = message;
        }
    }
}
