﻿namespace DTOs
{
    public class ErrorModel
    {
        public string Message { get; set; }
        public int StatusCode { get; set; }
        public string OriginalErrorMessage { get; set; }
    }
}
