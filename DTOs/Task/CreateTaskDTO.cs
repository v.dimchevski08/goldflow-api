﻿using System;

namespace DTOs
{
    public class CreateTaskDTO
    {
        public string Title { get; set; }
        public string Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ProjectId { get; set; }
        public string TeamMemberId { get; set; }
        public string Role { get; set; }
    }
}
