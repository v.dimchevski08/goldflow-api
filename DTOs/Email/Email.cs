﻿using System.Collections.Generic;

namespace DTOs
{
    public class Email
    {
        public string From { get; set; }
        public List<string> To { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
    }
}
