﻿namespace DTOs
{
    public class CreateCommentDTO
    {
        public string Text { get; set; }
        public string TaskId { get; set; }
    }
}
