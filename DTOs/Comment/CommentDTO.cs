﻿namespace DTOs
{
    public class CommentDTO
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string TaskId { get; set; }
    }
}
