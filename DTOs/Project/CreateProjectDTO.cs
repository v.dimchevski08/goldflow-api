﻿using System;

namespace DTOs
{
    public class CreateProjectDTO
    {
        public string Title { get; set; }
        public string Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ManagerId { get; set; }
    }
}
