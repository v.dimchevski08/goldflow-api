﻿namespace DTOs
{
    public class TeamDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ProjectId { get; set; }
    }
}
