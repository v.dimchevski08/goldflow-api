﻿namespace DTOs
{
    public class CreateTeamDTO
    {
        public string Name { get; set; }
        public string ProjectId { get; set; }
    }
}
